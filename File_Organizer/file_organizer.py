__author__ = 'winstark'

import scandir
import os
from pathlib2 import Path
import sys

DIRECTORIES = {
    "IMAGES": [".jpeg", ".jpg", ".tiff", ".gif", ".bmp", ".png", 
                ".bpg", ".svg", ".psd", "exif", ".ico"],
    "VIDEOS": [".mp4", ".avi", ".flv", ".mpeg", ".mkv", ".mov", ".webm", ".vob", ".wmv", ".3gp"],
    "AUDIO": [".mp3", ".m4a", ".wav", ".aa", ".acc", ".aiff", ".raw" ],
    "DOCUMENTS": [".xls", ".xlsx", ".doc", ".docx", ".ppt", ".pptx", ".epub", ".csv"],
    "ARCHIVES": [".iso",".tar", ".gz", ".rz", ".7z", ".dmg", ".rar", ".zar", ".zip"],
    "PDF": [".pdf"],
    "CODE": [".py", ".sh", ".cpp", ".js", ".c"],
    "HTML": [".html5", ".html", ".htm", ".xhtml"],
    "PLAINTEXT": [".txt", ".in", ".out", ".json", ".xml"],
    "EXE": [".exe"]
}
FILE_FORMATS = {file_format: directory
                for directory, file_formats in DIRECTORIES.items()
                for file_format in file_formats
                }


def organize_mess(path):
    try:
        dir = path + "OTHER_FILES"
        if not os.path.exists(dir):
            os.makedirs(dir)
        for entry in scandir.scandir(path=path):
            if entry.is_dir():
                continue
            file_path = Path(entry.name)
            file_format = file_path.suffix.lower()
            if file_format in FILE_FORMATS:
                directory_path = Path(path).joinpath(FILE_FORMATS[file_format])
                directory_path.mkdir(exist_ok=True)
                source = path + str(file_path)
                destination = str(directory_path) + "/" + str(file_path)
                os.rename(source, destination)
            else:
                source = path + "/" + str(file_path)
                destination = path + '/OTHER_FILES/' + str(file_path)
                os.rename(source, destination)
    except Exception, ex:
        print str(ex)
        pass
if __name__ == "__main__":
    dir = sys.argv[1]
    if not os.path.exists(str(dir)):
       print str(dir) + " is not exists"
       exit(1)
    organize_mess(str(dir))
